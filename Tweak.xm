// Made by CardboardFace
// Credit to Cuboid: https://github.com/ryannair05/Cuboid-1

static CGFloat topOffset = 200.0; // Icon list margin from screen top
static CGFloat verticalPadding = -5.0; // Vertical padding between icons

%hook SBIconListGridLayoutConfiguration 

	-(UIEdgeInsets)portraitLayoutInsets {
		UIEdgeInsets x = %orig;
		
		// Prevent tweak effecting folders
		NSUInteger rows = MSHookIvar<NSUInteger>(self, "_numberOfPortraitRows");
		NSUInteger columns = MSHookIvar<NSUInteger>(self, "_numberOfPortraitColumns");
		if (!(rows > 3 && columns == 4))
		{
			return x;
		}

		return UIEdgeInsetsMake(
			x.top + topOffset,
			x.left,
			x.bottom - topOffset + verticalPadding*2, // * 2 because regularly it was too slow
			x.right
		);
	}
%end