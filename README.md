# LowerIcons
Light-weight iOS tweak to lower the icons down the screen on the homescreen.

Credit to [Cuboid](https://github.com/KritantaDev/Cuboid).

**NOTE:** This is calibrated for an iPhone X